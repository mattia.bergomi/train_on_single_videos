from __future__ import absolute_import, division, print_function
import argparse
import os
import numpy as np
import cv2
import logging
import matplotlib.pyplot as plt
import idtrackerai
from idtrackerai.blob import Blob
from idtrackerai.list_of_blobs import ListOfBlobs
from idtrackerai.fragment import Fragment
from idtrackerai.network.identification_model.get_data import DataSet, split_data_train_and_validation
from idtrackerai.network.identification_model.network_params import NetworkParams
from idtrackerai.network.identification_model.id_CNN import ConvNetwork
from idtrackerai.network.identification_model.store_accuracy_and_loss import Store_Accuracy_and_Loss
from idtrackerai.network.identification_model.stop_training_criteria import Stop_Training
from idtrackerai.network.identification_model.epoch_runner import EpochRunner

def load_lists_of_blobs(path_to_blobs):
    #1. Load all the list of blobs
    paths = [f for f in os.listdir(path_to_blobs) if 'blobs_collection' in f]
    return [np.load(os.path.join(path_to_blobs, f)).item() for f in paths]

def assign_identities_to(lists_of_blobs):
    for i, list_of_blobs in enumerate(lists_of_blobs):
        [setattr(blob, '_identity', i + 1) for blobs_in_frame
         in list_of_blobs.blobs_in_video for blob in blobs_in_frame]

def create_fragments_from(lists_of_blobs):
    return [create_fragment(list_of_blobs, i, len(lists_of_blobs))
            for i, list_of_blobs in enumerate(lists_of_blobs)]

def create_fragment(list_of_blobs, identity, number_of_animals):
    images = [blob.image_for_identification for blobs_in_frame
              in list_of_blobs.blobs_in_video for blob in blobs_in_frame]
    individual_fragment = Fragment(fragment_identifier = identity,
                                   blob_hierarchy_in_starting_frame = identity,
                                   images = images, is_an_individual = True,
                                   is_a_crossing = False,
                                   number_of_animals = number_of_animals)
    individual_fragment.distance_travelled = 100
    return individual_fragment


def resize_if_necessary(individual_fragments):
    image_sizes = [fragment.images[0].shape[0] for fragment in individual_fragments]
    if np.std(image_sizes) != 0:
        min_image_size = min(image_sizes)

        for i, image_size in enumerate(image_sizes):
            if image_size != min_image_size:
                resampling_factor = min_image_size / image_size
                individual_fragments[i].images = resize_images(individual_fragments[i].images,
                                                  resampling_factor)
    return individual_fragments, individual_fragments[i].images[0].shape

def resize_images(images, resampling_factor):
    new_images = []

    for image in images:
        new_images.append(cv2.resize(image, None,
                           fx = resampling_factor,
                           fy = resampling_factor,
                           interpolation = cv2.INTER_CUBIC))

    return new_images

def generate_dataset(individual_fragments):
    individual_fragments, identification_image_size = resize_if_necessary(individual_fragments)
    images = np.concatenate([fragment.images for fragment in
                             individual_fragments], axis = 0)
    num_of_images = [len(fragment.images) for fragment in individual_fragments]
    labels = np.concatenate([[i for n in range(num_of_images)] for
                              i, num_of_images in enumerate(num_of_images)],
                              axis = 0)
    return images, labels

def train_model(net, images, labels, plot_flag = True):
    logging.info("Training...")
    store_training_accuracy_and_loss_data = Store_Accuracy_and_Loss(net, name = 'training', scope = 'training')
    store_validation_accuracy_and_loss_data = Store_Accuracy_and_Loss(net, name = 'validation', scope = 'training')
    if plot_flag:
        plt.ion()
        fig, ax_arr = plt.subplots(3)
        fig.canvas.set_window_title("Accuracy and loss")
        fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)
    # Instantiate data set
    training_dataset, validation_dataset = split_data_train_and_validation(net.params.number_of_animals,
                                                                           images,
                                                                           labels)
    # Convert labels to one hot vectors
    training_dataset.convert_labels_to_one_hot()
    validation_dataset.convert_labels_to_one_hot()
    # Reinitialize softmax and fully connected
    net.reinitialize_softmax_and_fully_connected()
    # Train network
    #compute weights to be fed to the loss function (weighted cross entropy)
    net.compute_loss_weights(training_dataset.labels)
    trainer = EpochRunner(training_dataset,
                        starting_epoch = 0,
                        print_flag = True,
                        batch_size = 50)
    validator = EpochRunner(validation_dataset,
                        starting_epoch = 0,
                        print_flag = True,
                        batch_size = 50)
    #set criteria to stop the training
    stop_training = Stop_Training(net.params.number_of_animals,
                                check_for_loss_plateau = True,
                                first_accumulation_flag = True)
    global_step = 0
    global_step0 = global_step

    while not stop_training(store_training_accuracy_and_loss_data,
                            store_validation_accuracy_and_loss_data,
                            trainer._epochs_completed):
        # --- Training
        feed_dict_train = trainer.run_epoch('Training', store_training_accuracy_and_loss_data, net.train)
        # --- Validation
        feed_dict_val = validator.run_epoch('Validation', store_validation_accuracy_and_loss_data, net.validate)
        # update global step
        net.session.run(net.global_step.assign(trainer.starting_epoch + trainer._epochs_completed))
        # Update counter
        trainer._epochs_completed += 1
        validator._epochs_completed += 1

    if (np.isnan(store_training_accuracy_and_loss_data.loss[-1]) or np.isnan(store_validation_accuracy_and_loss_data.loss[-1])):
        raise ValueError("The model diverged")
    else:
        global_step += trainer.epochs_completed
        logging.debug('loss values in validation: %s' %str(store_validation_accuracy_and_loss_data.loss[global_step0:]))
        # plot if asked
        if plot_flag:
            store_training_accuracy_and_loss_data.plot(ax_arr, color = 'r')
            store_validation_accuracy_and_loss_data.plot(ax_arr, color ='b')
        net.save()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--path_to_blobs", "-p" ,help="path to folder\
                        containing the lists of blobs", type = str)
    args = parser.parse_args()
    path_to_blobs = args.path_to_blobs
    lists_of_blobs = load_lists_of_blobs(path_to_blobs)
    number_of_animals = len(lists_of_blobs)
    assign_identities_to(lists_of_blobs)
    individual_fragments = create_fragments_from(lists_of_blobs)
    images, labels = generate_dataset(individual_fragments)
    network_params = NetworkParams(number_of_animals,
                        learning_rate = 0.005,
                        keep_prob = 1.0,
                        scopes_layers_to_optimize = None,
                        save_folder = path_to_blobs,
                        image_size = images[0].shape + (1,))
    net = ConvNetwork(network_params)
    train_model(net, images, labels, plot_flag = True)
